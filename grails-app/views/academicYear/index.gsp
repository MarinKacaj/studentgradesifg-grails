<%@ page import="studentgradesifgalphagrails.AcademicYear" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'academicYear.label', default: 'AcademicYear')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-academicYear" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                   default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-academicYear" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="endYear"
                              title="${message(code: 'academicYear.endYear.label', default: 'End Year')}"/>

            <g:sortableColumn property="startYear"
                              title="${message(code: 'academicYear.startYear.label', default: 'Start Year')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${academicYearInstanceList}" status="i" var="academicYearInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${academicYearInstance.id}">${fieldValue(bean: academicYearInstance, field: "endYear")}</g:link></td>

                <td>${fieldValue(bean: academicYearInstance, field: "startYear")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${academicYearInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
