<%@ page import="studentgradesifgalphagrails.AcademicYear" %>



<div class="fieldcontain ${hasErrors(bean: academicYearInstance, field: 'endYear', 'error')} ">
    <label for="endYear">
        <g:message code="academicYear.endYear.label" default="End Year"/>

    </label>
    <g:field name="endYear" type="number" value="${academicYearInstance.endYear}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: academicYearInstance, field: 'exams', 'error')} ">
    <label for="exams">
        <g:message code="academicYear.exams.label" default="Exams"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${academicYearInstance?.exams ?}" var="e">
            <li><g:link controller="exam" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="exam" action="create"
                    params="['academicYear.id': academicYearInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'exam.label', default: 'Exam')])}</g:link>
        </li>
    </ul>

</div>

<div class="fieldcontain ${hasErrors(bean: academicYearInstance, field: 'startYear', 'error')} required">
    <label for="startYear">
        <g:message code="academicYear.startYear.label" default="Start Year"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="startYear" type="number" value="${academicYearInstance.startYear}" required=""/>

</div>

