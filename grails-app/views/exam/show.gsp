<%@ page import="studentgradesifgalphagrails.Exam" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'exam.label', default: 'Exam')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-exam" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-exam" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list exam">

        <g:if test="${examInstance?.module}">
            <li class="fieldcontain">
                <span id="module-label" class="property-label"><g:message code="exam.module.label"
                                                                          default="Module"/></span>

                <span class="property-value" aria-labelledby="module-label"><g:fieldValue bean="${examInstance}"
                                                                                          field="module"/></span>

            </li>
        </g:if>

        <g:if test="${examInstance?.academicYear}">
            <li class="fieldcontain">
                <span id="academicYear-label" class="property-label"><g:message code="exam.academicYear.label"
                                                                                default="Academic Year"/></span>

                <span class="property-value" aria-labelledby="academicYear-label"><g:link controller="academicYear"
                                                                                          action="show"
                                                                                          id="${examInstance?.academicYear?.id}">${examInstance?.academicYear?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${examInstance?.mark}">
            <li class="fieldcontain">
                <span id="mark-label" class="property-label"><g:message code="exam.mark.label" default="Mark"/></span>

                <span class="property-value" aria-labelledby="mark-label"><g:fieldValue bean="${examInstance}"
                                                                                        field="mark"/></span>

            </li>
        </g:if>

        <g:if test="${examInstance?.professor}">
            <li class="fieldcontain">
                <span id="professor-label" class="property-label"><g:message code="exam.professor.label"
                                                                             default="Professor"/></span>

                <span class="property-value" aria-labelledby="professor-label"><g:link controller="professor"
                                                                                       action="show"
                                                                                       id="${examInstance?.professor?.id}">${examInstance?.professor?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${examInstance?.student}">
            <li class="fieldcontain">
                <span id="student-label" class="property-label"><g:message code="exam.student.label"
                                                                           default="Student"/></span>

                <span class="property-value" aria-labelledby="student-label"><g:link controller="student" action="show"
                                                                                     id="${examInstance?.student?.id}">${examInstance?.student?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${examInstance?.subject}">
            <li class="fieldcontain">
                <span id="subject-label" class="property-label"><g:message code="exam.subject.label"
                                                                           default="Subject"/></span>

                <span class="property-value" aria-labelledby="subject-label"><g:link controller="subject" action="show"
                                                                                     id="${examInstance?.subject?.id}">${examInstance?.subject?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: examInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${examInstance}"><g:message code="default.button.edit.label"
                                                                                     default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
