<%@ page import="studentgradesifgalphagrails.Exam" %>



<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'module', 'error')} required">
    <label for="module">
        <g:message code="exam.module.label" default="Module"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textArea name="module" cols="40" rows="5" maxlength="255" required="" value="${examInstance?.module}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'academicYear', 'error')} required">
    <label for="academicYear">
        <g:message code="exam.academicYear.label" default="Academic Year"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="academicYear" name="academicYear.id" from="${studentgradesifgalphagrails.AcademicYear.list()}"
              optionKey="id" required="" value="${examInstance?.academicYear?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'mark', 'error')} required">
    <label for="mark">
        <g:message code="exam.mark.label" default="Mark"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="mark" type="number" value="${examInstance.mark}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'professor', 'error')} required">
    <label for="professor">
        <g:message code="exam.professor.label" default="Professor"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="professor" name="professor.id" from="${studentgradesifgalphagrails.Professor.list()}" optionKey="id"
              required="" value="${examInstance?.professor?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'student', 'error')} required">
    <label for="student">
        <g:message code="exam.student.label" default="Student"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="student" name="student.id" from="${studentgradesifgalphagrails.Student.list()}" optionKey="id"
              required="" value="${examInstance?.student?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'subject', 'error')} required">
    <label for="subject">
        <g:message code="exam.subject.label" default="Subject"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="subject" name="subject.id" from="${studentgradesifgalphagrails.Subject.list()}" optionKey="id"
              required="" value="${examInstance?.subject?.id}" class="many-to-one"/>

</div>

