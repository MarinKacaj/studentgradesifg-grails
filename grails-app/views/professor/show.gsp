<%@ page import="studentgradesifgalphagrails.Professor" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'professor.label', default: 'Professor')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-professor" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-professor" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list professor">

        <g:if test="${professorInstance?.teachingEndYear}">
            <li class="fieldcontain">
                <span id="teachingEndYear-label" class="property-label"><g:message
                        code="professor.teachingEndYear.label" default="Teaching End Year"/></span>

                <span class="property-value" aria-labelledby="teachingEndYear-label"><g:fieldValue
                        bean="${professorInstance}" field="teachingEndYear"/></span>

            </li>
        </g:if>

        <g:if test="${professorInstance?.email}">
            <li class="fieldcontain">
                <span id="email-label" class="property-label"><g:message code="professor.email.label"
                                                                         default="Email"/></span>

                <span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${professorInstance}"
                                                                                         field="email"/></span>

            </li>
        </g:if>

        <g:if test="${professorInstance?.establishment}">
            <li class="fieldcontain">
                <span id="establishment-label" class="property-label"><g:message code="professor.establishment.label"
                                                                                 default="Establishment"/></span>

                <span class="property-value" aria-labelledby="establishment-label"><g:fieldValue
                        bean="${professorInstance}" field="establishment"/></span>

            </li>
        </g:if>

        <g:if test="${professorInstance?.exams}">
            <li class="fieldcontain">
                <span id="exams-label" class="property-label"><g:message code="professor.exams.label"
                                                                         default="Exams"/></span>

                <g:each in="${professorInstance.exams}" var="e">
                    <span class="property-value" aria-labelledby="exams-label"><g:link controller="exam" action="show"
                                                                                       id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

        <g:if test="${professorInstance?.fullName}">
            <li class="fieldcontain">
                <span id="fullName-label" class="property-label"><g:message code="professor.fullName.label"
                                                                            default="Full Name"/></span>

                <span class="property-value" aria-labelledby="fullName-label"><g:fieldValue bean="${professorInstance}"
                                                                                            field="fullName"/></span>

            </li>
        </g:if>

        <g:if test="${professorInstance?.teachingStartYear}">
            <li class="fieldcontain">
                <span id="teachingStartYear-label" class="property-label"><g:message
                        code="professor.teachingStartYear.label" default="Teaching Start Year"/></span>

                <span class="property-value" aria-labelledby="teachingStartYear-label"><g:fieldValue
                        bean="${professorInstance}" field="teachingStartYear"/></span>

            </li>
        </g:if>

        <g:if test="${professorInstance?.telNum}">
            <li class="fieldcontain">
                <span id="telNum-label" class="property-label"><g:message code="professor.telNum.label"
                                                                          default="Tel Num"/></span>

                <span class="property-value" aria-labelledby="telNum-label"><g:fieldValue bean="${professorInstance}"
                                                                                          field="telNum"/></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: professorInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${professorInstance}"><g:message
                    code="default.button.edit.label" default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
