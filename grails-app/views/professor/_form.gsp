<%@ page import="studentgradesifgalphagrails.Professor" %>



<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'teachingEndYear', 'error')} ">
    <label for="teachingEndYear">
        <g:message code="professor.teachingEndYear.label" default="Teaching End Year"/>

    </label>
    <g:field name="teachingEndYear" type="number" value="${professorInstance.teachingEndYear}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'email', 'error')} required">
    <label for="email">
        <g:message code="professor.email.label" default="Email"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="email" required="" value="${professorInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'establishment', 'error')} required">
    <label for="establishment">
        <g:message code="professor.establishment.label" default="Establishment"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="establishment" required="" value="${professorInstance?.establishment}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'exams', 'error')} ">
    <label for="exams">
        <g:message code="professor.exams.label" default="Exams"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${professorInstance?.exams ?}" var="e">
            <li><g:link controller="exam" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="exam" action="create"
                    params="['professor.id': professorInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'exam.label', default: 'Exam')])}</g:link>
        </li>
    </ul>

</div>

<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'fullName', 'error')} required">
    <label for="fullName">
        <g:message code="professor.fullName.label" default="Full Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="fullName" required="" value="${professorInstance?.fullName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'teachingStartYear', 'error')} required">
    <label for="teachingStartYear">
        <g:message code="professor.teachingStartYear.label" default="Teaching Start Year"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="teachingStartYear" type="number" value="${professorInstance.teachingStartYear}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: professorInstance, field: 'telNum', 'error')} required">
    <label for="telNum">
        <g:message code="professor.telNum.label" default="Tel Num"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="telNum" required="" value="${professorInstance?.telNum}"/>

</div>

