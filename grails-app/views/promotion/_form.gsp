<%@ page import="studentgradesifgalphagrails.Promotion" %>



<div class="fieldcontain ${hasErrors(bean: promotionInstance, field: 'promotion', 'error')} required">
    <label for="promotion">
        <g:message code="promotion.promotion.label" default="Promotion"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="promotion" required="" value="${promotionInstance?.promotion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: promotionInstance, field: 'students', 'error')} ">
    <label for="students">
        <g:message code="promotion.students.label" default="Students"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${promotionInstance?.students ?}" var="s">
            <li><g:link controller="student" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="student" action="create"
                    params="['promotion.id': promotionInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'student.label', default: 'Student')])}</g:link>
        </li>
    </ul>

</div>

