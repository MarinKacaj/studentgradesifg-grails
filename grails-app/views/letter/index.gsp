<%@ page import="studentgradesifgalphagrails.Letter" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'letter.label', default: 'Letter')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-letter" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                             default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-letter" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="content" title="${message(code: 'letter.content.label', default: 'Content')}"/>

            <th><g:message code="letter.student.label" default="Student"/></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${letterInstanceList}" status="i" var="letterInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${letterInstance.id}">${fieldValue(bean: letterInstance, field: "content")}</g:link></td>

                <td>${fieldValue(bean: letterInstance, field: "student")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${letterInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
