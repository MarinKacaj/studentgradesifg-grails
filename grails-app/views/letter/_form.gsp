<%@ page import="studentgradesifgalphagrails.Letter" %>



<div class="fieldcontain ${hasErrors(bean: letterInstance, field: 'content', 'error')} required">
    <label for="content">
        <g:message code="letter.content.label" default="Content"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="content" required="" value="${letterInstance?.content}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: letterInstance, field: 'student', 'error')} required">
    <label for="student">
        <g:message code="letter.student.label" default="Student"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="student" name="student.id" from="${studentgradesifgalphagrails.Student.list()}" optionKey="id"
              required="" value="${letterInstance?.student?.id}" class="many-to-one"/>

</div>

