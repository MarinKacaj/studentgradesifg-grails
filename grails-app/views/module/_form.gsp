<%@ page import="studentgradesifgalphagrails.Module" %>



<div class="fieldcontain ${hasErrors(bean: moduleInstance, field: 'coefficient', 'error')} required">
    <label for="coefficient">
        <g:message code="module.coefficient.label" default="Coefficient"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="coefficient" value="${fieldValue(bean: moduleInstance, field: 'coefficient')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: moduleInstance, field: 'module', 'error')} required">
    <label for="module">
        <g:message code="module.module.label" default="Module"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="module" required="" value="${moduleInstance?.module}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: moduleInstance, field: 'subjects', 'error')} ">
    <label for="subjects">
        <g:message code="module.subjects.label" default="Subjects"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${moduleInstance?.subjects ?}" var="s">
            <li><g:link controller="subject" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="subject" action="create"
                    params="['module.id': moduleInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'subject.label', default: 'Subject')])}</g:link>
        </li>
    </ul>

</div>

