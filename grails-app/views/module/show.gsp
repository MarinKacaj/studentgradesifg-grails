<%@ page import="studentgradesifgalphagrails.Module" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'module.label', default: 'Module')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-module" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                             default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-module" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list module">

        <g:if test="${moduleInstance?.coefficient}">
            <li class="fieldcontain">
                <span id="coefficient-label" class="property-label"><g:message code="module.coefficient.label"
                                                                               default="Coefficient"/></span>

                <span class="property-value" aria-labelledby="coefficient-label"><g:fieldValue bean="${moduleInstance}"
                                                                                               field="coefficient"/></span>

            </li>
        </g:if>

        <g:if test="${moduleInstance?.module}">
            <li class="fieldcontain">
                <span id="module-label" class="property-label"><g:message code="module.module.label"
                                                                          default="Module"/></span>

                <span class="property-value" aria-labelledby="module-label"><g:fieldValue bean="${moduleInstance}"
                                                                                          field="module"/></span>

            </li>
        </g:if>

        <g:if test="${moduleInstance?.subjects}">
            <li class="fieldcontain">
                <span id="subjects-label" class="property-label"><g:message code="module.subjects.label"
                                                                            default="Subjects"/></span>

                <g:each in="${moduleInstance.subjects}" var="s">
                    <span class="property-value" aria-labelledby="subjects-label"><g:link controller="subject"
                                                                                          action="show"
                                                                                          id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: moduleInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${moduleInstance}"><g:message code="default.button.edit.label"
                                                                                       default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
