<%@ page import="studentgradesifgalphagrails.Subject" %>



<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'exams', 'error')} ">
    <label for="exams">
        <g:message code="subject.exams.label" default="Exams"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${subjectInstance?.exams ?}" var="e">
            <li><g:link controller="exam" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="exam" action="create"
                    params="['subject.id': subjectInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'exam.label', default: 'Exam')])}</g:link>
        </li>
    </ul>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'module', 'error')} required">
    <label for="module">
        <g:message code="subject.module.label" default="Module"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="module" name="module.id" from="${studentgradesifgalphagrails.Module.list()}" optionKey="id"
              required="" value="${subjectInstance?.module?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="subject.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${subjectInstance?.name}"/>

</div>

