<%@ page import="studentgradesifgalphagrails.StudyPeriod" %>



<div class="fieldcontain ${hasErrors(bean: studyPeriodInstance, field: 'endYear', 'error')} ">
    <label for="endYear">
        <g:message code="studyPeriod.endYear.label" default="End Year"/>

    </label>
    <g:field name="endYear" type="number" value="${studyPeriodInstance.endYear}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studyPeriodInstance, field: 'startYear', 'error')} required">
    <label for="startYear">
        <g:message code="studyPeriod.startYear.label" default="Start Year"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="startYear" type="number" value="${studyPeriodInstance.startYear}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: studyPeriodInstance, field: 'students', 'error')} ">
    <label for="students">
        <g:message code="studyPeriod.students.label" default="Students"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${studyPeriodInstance?.students ?}" var="s">
            <li><g:link controller="student" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="student" action="create"
                    params="['studyPeriod.id': studyPeriodInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'student.label', default: 'Student')])}</g:link>
        </li>
    </ul>

</div>

