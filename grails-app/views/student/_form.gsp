<%@ page import="studentgradesifgalphagrails.Student" %>



<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'gender', 'error')} required">
    <label for="gender">
        <g:message code="student.gender.label" default="Gender"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="gender" maxlength="1" required="" value="${studentInstance?.gender}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'address', 'error')} required">
    <label for="address">
        <g:message code="student.address.label" default="Address"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="address" required="" value="${studentInstance?.address}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'birthYear', 'error')} required">
    <label for="birthYear">
        <g:message code="student.birthYear.label" default="Birth Year"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="birthYear" type="number" value="${studentInstance.birthYear}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'city', 'error')} required">
    <label for="city">
        <g:message code="student.city.label" default="City"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="city" required="" value="${studentInstance?.city}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'country', 'error')} required">
    <label for="country">
        <g:message code="student.country.label" default="Country"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="country" required="" value="${studentInstance?.country}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'email', 'error')} required">
    <label for="email">
        <g:message code="student.email.label" default="Email"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="email" required="" value="${studentInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'exams', 'error')} ">
    <label for="exams">
        <g:message code="student.exams.label" default="Exams"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${studentInstance?.exams ?}" var="e">
            <li><g:link controller="exam" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="exam" action="create"
                    params="['student.id': studentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'exam.label', default: 'Exam')])}</g:link>
        </li>
    </ul>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'fullName', 'error')} required">
    <label for="fullName">
        <g:message code="student.fullName.label" default="Full Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="fullName" required="" value="${studentInstance?.fullName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'initalFormation', 'error')} required">
    <label for="initalFormation">
        <g:message code="student.initalFormation.label" default="Inital Formation"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="initalFormation" name="initalFormation.id"
              from="${studentgradesifgalphagrails.InitialFormation.list()}" optionKey="id" required=""
              value="${studentInstance?.initalFormation?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'letters', 'error')} ">
    <label for="letters">
        <g:message code="student.letters.label" default="Letters"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${studentInstance?.letters ?}" var="l">
            <li><g:link controller="letter" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="letter" action="create"
                    params="['student.id': studentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'letter.label', default: 'Letter')])}</g:link>
        </li>
    </ul>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'mode', 'error')} required">
    <label for="mode">
        <g:message code="student.mode.label" default="Mode"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="mode" required="" value="${studentInstance?.mode}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'photo', 'error')} required">
    <label for="photo">
        <g:message code="student.photo.label" default="Photo"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="photo" required="" value="${studentInstance?.photo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'proSituation', 'error')} required">
    <label for="proSituation">
        <g:message code="student.proSituation.label" default="Pro Situation"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="proSituation" required="" value="${studentInstance?.proSituation}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'promotion', 'error')} required">
    <label for="promotion">
        <g:message code="student.promotion.label" default="Promotion"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="promotion" name="promotion.id" from="${studentgradesifgalphagrails.Promotion.list()}" optionKey="id"
              required="" value="${studentInstance?.promotion?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'studyPeriod', 'error')} required">
    <label for="studyPeriod">
        <g:message code="student.studyPeriod.label" default="Study Period"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="studyPeriod" name="studyPeriod.id" from="${studentgradesifgalphagrails.StudyPeriod.list()}"
              optionKey="id" required="" value="${studentInstance?.studyPeriod?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'success', 'error')} required">
    <label for="success">
        <g:message code="student.success.label" default="Success"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="success" required="" value="${studentInstance?.success}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'telNum', 'error')} required">
    <label for="telNum">
        <g:message code="student.telNum.label" default="Tel Num"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="telNum" required="" value="${studentInstance?.telNum}"/>

</div>

