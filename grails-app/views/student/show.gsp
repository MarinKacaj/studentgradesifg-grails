<%@ page import="studentgradesifgalphagrails.Student" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-student" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-student" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list student">

        <g:if test="${studentInstance?.gender}">
            <li class="fieldcontain">
                <span id="gender-label" class="property-label"><g:message code="student.gender.label"
                                                                          default="Gender"/></span>

                <span class="property-value" aria-labelledby="gender-label"><g:fieldValue bean="${studentInstance}"
                                                                                          field="gender"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.address}">
            <li class="fieldcontain">
                <span id="address-label" class="property-label"><g:message code="student.address.label"
                                                                           default="Address"/></span>

                <span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${studentInstance}"
                                                                                           field="address"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.birthYear}">
            <li class="fieldcontain">
                <span id="birthYear-label" class="property-label"><g:message code="student.birthYear.label"
                                                                             default="Birth Year"/></span>

                <span class="property-value" aria-labelledby="birthYear-label"><g:fieldValue bean="${studentInstance}"
                                                                                             field="birthYear"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.city}">
            <li class="fieldcontain">
                <span id="city-label" class="property-label"><g:message code="student.city.label"
                                                                        default="City"/></span>

                <span class="property-value" aria-labelledby="city-label"><g:fieldValue bean="${studentInstance}"
                                                                                        field="city"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.country}">
            <li class="fieldcontain">
                <span id="country-label" class="property-label"><g:message code="student.country.label"
                                                                           default="Country"/></span>

                <span class="property-value" aria-labelledby="country-label"><g:fieldValue bean="${studentInstance}"
                                                                                           field="country"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.email}">
            <li class="fieldcontain">
                <span id="email-label" class="property-label"><g:message code="student.email.label"
                                                                         default="Email"/></span>

                <span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${studentInstance}"
                                                                                         field="email"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.exams}">
            <li class="fieldcontain">
                <span id="exams-label" class="property-label"><g:message code="student.exams.label"
                                                                         default="Exams"/></span>

                <g:each in="${studentInstance.exams}" var="e">
                    <span class="property-value" aria-labelledby="exams-label"><g:link controller="exam" action="show"
                                                                                       id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

        <g:if test="${studentInstance?.fullName}">
            <li class="fieldcontain">
                <span id="fullName-label" class="property-label"><g:message code="student.fullName.label"
                                                                            default="Full Name"/></span>

                <span class="property-value" aria-labelledby="fullName-label"><g:fieldValue bean="${studentInstance}"
                                                                                            field="fullName"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.initalFormation}">
            <li class="fieldcontain">
                <span id="initalFormation-label" class="property-label"><g:message code="student.initalFormation.label"
                                                                                   default="Inital Formation"/></span>

                <span class="property-value" aria-labelledby="initalFormation-label"><g:link
                        controller="initialFormation" action="show"
                        id="${studentInstance?.initalFormation?.id}">${studentInstance?.initalFormation?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.letters}">
            <li class="fieldcontain">
                <span id="letters-label" class="property-label"><g:message code="student.letters.label"
                                                                           default="Letters"/></span>

                <g:each in="${studentInstance.letters}" var="l">
                    <span class="property-value" aria-labelledby="letters-label"><g:link controller="letter"
                                                                                         action="show"
                                                                                         id="${l.id}">${l?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

        <g:if test="${studentInstance?.mode}">
            <li class="fieldcontain">
                <span id="mode-label" class="property-label"><g:message code="student.mode.label"
                                                                        default="Mode"/></span>

                <span class="property-value" aria-labelledby="mode-label"><g:fieldValue bean="${studentInstance}"
                                                                                        field="mode"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.photo}">
            <li class="fieldcontain">
                <span id="photo-label" class="property-label"><g:message code="student.photo.label"
                                                                         default="Photo"/></span>

                <span class="property-value" aria-labelledby="photo-label"><g:fieldValue bean="${studentInstance}"
                                                                                         field="photo"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.proSituation}">
            <li class="fieldcontain">
                <span id="proSituation-label" class="property-label"><g:message code="student.proSituation.label"
                                                                                default="Pro Situation"/></span>

                <span class="property-value" aria-labelledby="proSituation-label"><g:fieldValue
                        bean="${studentInstance}" field="proSituation"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.promotion}">
            <li class="fieldcontain">
                <span id="promotion-label" class="property-label"><g:message code="student.promotion.label"
                                                                             default="Promotion"/></span>

                <span class="property-value" aria-labelledby="promotion-label"><g:link controller="promotion"
                                                                                       action="show"
                                                                                       id="${studentInstance?.promotion?.id}">${studentInstance?.promotion?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.studyPeriod}">
            <li class="fieldcontain">
                <span id="studyPeriod-label" class="property-label"><g:message code="student.studyPeriod.label"
                                                                               default="Study Period"/></span>

                <span class="property-value" aria-labelledby="studyPeriod-label"><g:link controller="studyPeriod"
                                                                                         action="show"
                                                                                         id="${studentInstance?.studyPeriod?.id}">${studentInstance?.studyPeriod?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.success}">
            <li class="fieldcontain">
                <span id="success-label" class="property-label"><g:message code="student.success.label"
                                                                           default="Success"/></span>

                <span class="property-value" aria-labelledby="success-label"><g:fieldValue bean="${studentInstance}"
                                                                                           field="success"/></span>

            </li>
        </g:if>

        <g:if test="${studentInstance?.telNum}">
            <li class="fieldcontain">
                <span id="telNum-label" class="property-label"><g:message code="student.telNum.label"
                                                                          default="Tel Num"/></span>

                <span class="property-value" aria-labelledby="telNum-label"><g:fieldValue bean="${studentInstance}"
                                                                                          field="telNum"/></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: studentInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${studentInstance}"><g:message code="default.button.edit.label"
                                                                                        default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
