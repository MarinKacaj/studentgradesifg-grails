<%@ page import="studentgradesifgalphagrails.Student" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-student" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-student" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="gender" title="${message(code: 'student.gender.label', default: 'Gender')}"/>

            <g:sortableColumn property="address" title="${message(code: 'student.address.label', default: 'Address')}"/>

            <g:sortableColumn property="birthYear"
                              title="${message(code: 'student.birthYear.label', default: 'Birth Year')}"/>

            <g:sortableColumn property="city" title="${message(code: 'student.city.label', default: 'City')}"/>

            <g:sortableColumn property="country" title="${message(code: 'student.country.label', default: 'Country')}"/>

            <g:sortableColumn property="email" title="${message(code: 'student.email.label', default: 'Email')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${studentInstanceList}" status="i" var="studentInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${studentInstance.id}">${fieldValue(bean: studentInstance, field: "gender")}</g:link></td>

                <td>${fieldValue(bean: studentInstance, field: "address")}</td>

                <td>${fieldValue(bean: studentInstance, field: "birthYear")}</td>

                <td>${fieldValue(bean: studentInstance, field: "city")}</td>

                <td>${fieldValue(bean: studentInstance, field: "country")}</td>

                <td>${fieldValue(bean: studentInstance, field: "email")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${studentInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
