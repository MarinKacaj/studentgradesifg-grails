<%@ page import="studentgradesifgalphagrails.InitialFormation" %>



<div class="fieldcontain ${hasErrors(bean: initialFormationInstance, field: 'initialFormation', 'error')} required">
    <label for="initialFormation">
        <g:message code="initialFormation.initialFormation.label" default="Initial Formation"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="initialFormation" required="" value="${initialFormationInstance?.initialFormation}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: initialFormationInstance, field: 'students', 'error')} ">
    <label for="students">
        <g:message code="initialFormation.students.label" default="Students"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${initialFormationInstance?.students ?}" var="s">
            <li><g:link controller="student" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="student" action="create"
                    params="['initialFormation.id': initialFormationInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'student.label', default: 'Student')])}</g:link>
        </li>
    </ul>

</div>

