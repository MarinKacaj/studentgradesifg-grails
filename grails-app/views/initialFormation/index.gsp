<%@ page import="studentgradesifgalphagrails.InitialFormation" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'initialFormation.label', default: 'InitialFormation')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-initialFormation" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                       default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-initialFormation" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="initialFormation"
                              title="${message(code: 'initialFormation.initialFormation.label', default: 'Initial Formation')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${initialFormationInstanceList}" status="i" var="initialFormationInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${initialFormationInstance.id}">${fieldValue(bean: initialFormationInstance, field: "initialFormation")}</g:link></td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${initialFormationInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
