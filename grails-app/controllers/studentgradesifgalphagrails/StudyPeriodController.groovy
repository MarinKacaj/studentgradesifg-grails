package studentgradesifgalphagrails

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class StudyPeriodController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond StudyPeriod.list(params), model: [studyPeriodInstanceCount: StudyPeriod.count()]
    }

    def show(StudyPeriod studyPeriodInstance) {
        respond studyPeriodInstance
    }

    def create() {
        respond new StudyPeriod(params)
    }

    @Transactional
    def save(StudyPeriod studyPeriodInstance) {
        if (studyPeriodInstance == null) {
            notFound()
            return
        }

        if (studyPeriodInstance.hasErrors()) {
            respond studyPeriodInstance.errors, view: 'create'
            return
        }

        studyPeriodInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'studyPeriod.label', default: 'StudyPeriod'), studyPeriodInstance.id])
                redirect studyPeriodInstance
            }
            '*' { respond studyPeriodInstance, [status: CREATED] }
        }
    }

    def edit(StudyPeriod studyPeriodInstance) {
        respond studyPeriodInstance
    }

    @Transactional
    def update(StudyPeriod studyPeriodInstance) {
        if (studyPeriodInstance == null) {
            notFound()
            return
        }

        if (studyPeriodInstance.hasErrors()) {
            respond studyPeriodInstance.errors, view: 'edit'
            return
        }

        studyPeriodInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'StudyPeriod.label', default: 'StudyPeriod'), studyPeriodInstance.id])
                redirect studyPeriodInstance
            }
            '*' { respond studyPeriodInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(StudyPeriod studyPeriodInstance) {

        if (studyPeriodInstance == null) {
            notFound()
            return
        }

        studyPeriodInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'StudyPeriod.label', default: 'StudyPeriod'), studyPeriodInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'studyPeriod.label', default: 'StudyPeriod'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
