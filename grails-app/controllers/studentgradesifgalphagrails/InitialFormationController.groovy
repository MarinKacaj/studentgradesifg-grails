package studentgradesifgalphagrails

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class InitialFormationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond InitialFormation.list(params), model: [initialFormationInstanceCount: InitialFormation.count()]
    }

    def show(InitialFormation initialFormationInstance) {
        respond initialFormationInstance
    }

    def create() {
        respond new InitialFormation(params)
    }

    @Transactional
    def save(InitialFormation initialFormationInstance) {
        if (initialFormationInstance == null) {
            notFound()
            return
        }

        if (initialFormationInstance.hasErrors()) {
            respond initialFormationInstance.errors, view: 'create'
            return
        }

        initialFormationInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'initialFormation.label', default: 'InitialFormation'), initialFormationInstance.id])
                redirect initialFormationInstance
            }
            '*' { respond initialFormationInstance, [status: CREATED] }
        }
    }

    def edit(InitialFormation initialFormationInstance) {
        respond initialFormationInstance
    }

    @Transactional
    def update(InitialFormation initialFormationInstance) {
        if (initialFormationInstance == null) {
            notFound()
            return
        }

        if (initialFormationInstance.hasErrors()) {
            respond initialFormationInstance.errors, view: 'edit'
            return
        }

        initialFormationInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'InitialFormation.label', default: 'InitialFormation'), initialFormationInstance.id])
                redirect initialFormationInstance
            }
            '*' { respond initialFormationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(InitialFormation initialFormationInstance) {

        if (initialFormationInstance == null) {
            notFound()
            return
        }

        initialFormationInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'InitialFormation.label', default: 'InitialFormation'), initialFormationInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'initialFormation.label', default: 'InitialFormation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
