package studentgradesifgalphagrails

class Student {

    String fullName
    String gender
    Integer birthYear
    String address
    String city
    String country
    String telNum
    String email
    String proSituation
    String success
    String mode
    String photo // TODO - refactor

    static constraints = {
        gender maxSize: 1
    }

    static hasMany = [letters: Letter, exams: Exam]
    static hasOne = [initalFormation: InitialFormation, promotion: Promotion, studyPeriod: StudyPeriod]
}
