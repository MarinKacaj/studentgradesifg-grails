package studentgradesifgalphagrails

class StudyPeriod {

    Integer startYear
    Integer endYear

    static constraints = {
        endYear nullable: true
    }

    static hasMany = [students: Student]
}
