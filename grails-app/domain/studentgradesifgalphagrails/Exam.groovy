package studentgradesifgalphagrails

class Exam {

    Integer mark
    String module

    static constraints = {
        module maxSize: 255
    }

    static belongsTo = [student: Student, academicYear: AcademicYear, professor: Professor, subject: Subject]
}
