package studentgradesifgalphagrails

class Letter {

    String content

    static constraints = {
    }

    static belongsTo = [student: Student]
}
