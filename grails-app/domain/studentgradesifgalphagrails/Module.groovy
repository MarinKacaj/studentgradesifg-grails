package studentgradesifgalphagrails

class Module {

    String module
    Float coefficient

    static constraints = {
    }

    static mapping = {
        coefficient defaultValue: 0.0f
    }

    static hasMany = [subjects: Subject]
}
