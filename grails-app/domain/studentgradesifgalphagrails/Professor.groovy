package studentgradesifgalphagrails

class Professor {

    String fullName
    String establishment
    String email
    String telNum
    Integer teachingStartYear
    Integer teachingEndYear

    static constraints = {
        teachingEndYear nullable: true
    }

    static hasMany = [exams: Exam]
}
