package studentgradesifgalphagrails

class AcademicYear {

    Integer startYear
    Integer endYear

    static constraints = {
        endYear nullable: true
    }

    static hasMany = [exams: Exam]

    static namedQueries = {
        infraRedStuff {
            eq('startYear', 2000)
        }
    }
}
