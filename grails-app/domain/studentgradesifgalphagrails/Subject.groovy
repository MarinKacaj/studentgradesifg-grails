package studentgradesifgalphagrails

class Subject {

    String name

    static constraints = {
    }

    static hasMany = [exams: Exam]
    static belongsTo = [module: Module]
}
